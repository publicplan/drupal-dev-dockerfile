FROM drupal:php7.4-fpm-buster
RUN apt-get update && apt-get install -y ssh-client mariadb-client screen git wget rsync unzip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sS https://platform.sh/cli/installer | php
RUN echo 'memory_limit = 512M' >> /usr/local/etc/php/php.ini
RUN curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.35.2/install.sh -o install_nvm.sh
RUN bash install_nvm.sh
RUN wget https://github.com/git-lfs/git-lfs/releases/download/v2.9.0/git-lfs-linux-amd64-v2.9.0.tar.gz
RUN tar -xf git-lfs-linux-amd64-v2.9.0.tar.gz
RUN chmod 755 install.sh
RUN ./install.sh
